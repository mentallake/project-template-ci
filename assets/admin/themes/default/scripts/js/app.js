var toastr;

$(function(){
	initAnimateCss();
	initSideMenu();
	initElements();
});

function initElements(){
	if($('select:visible').length > 0){
		$('select:visible').selectpicker({
		  	size: 7
		});
	}

	$('.clearable-textbox').after('<a class="clear-text-btn">&times;</a>');

    $('.clear-text-btn').click(function(){
        $(this).prev('.clearable-textbox').val('');
        $(this).prev('.clearable-textbox').removeClass('has-value');

        if($(this).prev('.clearable-textbox').hasClass('number-only') ||
           $(this).prev('.clearable-textbox').attr('type') == 'number'){
            $(this).prev('.clearable-textbox').val('0');
        }
    });

    $('.clearable-textbox').on("change paste keyup", function() {
    	if($(this).val() != ''){
    		$(this).addClass('has-value');
    	}else{
    		$(this).removeClass('has-value');
    	}
    });

    $('.clearable-textbox').change();

	// $('[data-toggle="tooltip"]').tooltip();

	$('.number-only').keypress(function(evt){
	    if (evt.which < 48 || evt.which > 57){
	        evt.preventDefault();
	    }
	});

	$('.alphanumeric-only').keypress(function(evt){
    	var alphanumericRegex = /^[A-Za-z0-9]+$/;
        var specialCharacters = /^[~!@#$%^&*()_+-=`{}\[\]\|\\:;'<>,.\/? ]+$/;

        if($(this).hasClass('allow-space')){
            specialCharacters = /^[~!@#$%^&*()_+-=`{}\[\]\|\\:;'<>,.\/?]+$/;
        }

    	if(specialCharacters.test(evt.key)){
	        evt.preventDefault();
	    }
	});

    $('.alphanumeric-en-only').keypress(function(evt){
        var alphanumericRegex = /^[A-Za-z0-9]+$/;

        if(!alphanumericRegex.test(evt.key)){
            evt.preventDefault();
        }
    });

	initToastr();
}

function initAnimateCss(){
	$.fn.extend({
	    animateCss: function (animationName, type) {
	    	if(type == 'in'){
            	$(this).removeClass('hide');
            	$(this).css('opacity', '1');
            }

	        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
	        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
	            $(this).removeClass('animated ' + animationName);

	            if(type == 'out'){
	            	$(this).css('opacity', '0');
	            	$(this).addClass('hide');
	            }
	        });
	    }
	});
}

function isFieldCompleted(panelId){
	var $currentStepPanel = $(panelId);
	var isFieldCompleted = true;

	$currentStepPanel.find('.error').removeClass('error');

	$currentStepPanel.find('.required').each(function(){
        // If it is textbox, textarea.
        if($(this).is('input, textarea')){
            if($(this).val() == ''){
                isFieldCompleted = false;
                $(this).addClass('error');
            }
        }
    });

    if(!isFieldCompleted){
        $currentStepPanel.find('.error').eq(0).focus();
        $currentStepPanel.find('.error').animateCss('pulse', 'in');
    }

    return isFieldCompleted;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }

    return rtn;
}

function initToastr(){
    if(toastr){
    	toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-bottom-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "2500",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        var title = getUrlParameter('title');
    	var msg = getUrlParameter('msg');
    	var type = getUrlParameter('type');

    	showNoticeMessage(title, msg, type);
    }
}

function showNoticeMessage(title, message, type){
	if (message) {
		// Show notice message
	    // toastr.remove();

	    if(title){
			if(type && type == 'error'){
				toastr.error(message, title);
			}else{
				toastr.success(message, title);
			}
		}else{
			// Display an info toast with no title
			if(type && type == 'error'){
				toastr.error(message);
			}else{
				toastr.success(message);
			}
		}

	    // Clear query string
		var currentUrl = window.location.href;
		currentUrl = removeParam("title", currentUrl);
		currentUrl = removeParam("msg", currentUrl);
		currentUrl = removeParam("type", currentUrl);

	    window.history.pushState("","", currentUrl);
	}
}

function initSideMenu(){
	initSideMenuHeight();
	initSideMenuItem();
}

function initSideMenuHeight(){
	if($('.main-content-col').length > 0){
		$('.main-content-col').matchHeight();
	}
}

function initSideMenuItem(){
	$(".side-menu li.has-child > a").click(function(){
		var $submenu = $(this).siblings('.sub-menu');
		$submenu.collapse('toggle');
		$(this).toggleClass('opened');
	});

	$('#menu-btn').click(function(){
		$('#side-menu-panel').toggleClass('shrink');
		$('#main-content-panel').toggleClass('expand');
	});
}

function showItemLoadingPanel(){
    $('#item-loading-panel').show();
}

function hideItemLoadingPanel(){
    $('#item-loading-panel').hide();
}

function showLoadingPanel(){
    $('#loading-panel').show();
}

function hideLoadingPanel(){
    $('#loading-panel').hide();
}

function readURL(input){
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var imgContainer = $(input).parents('.image-container');

        reader.onload = function (e) {
            $(imgContainer).css('background-image', 'url("' + e.target.result + '")');
            $(input).parent().addClass('has-image');
        }

        reader.readAsDataURL(input.files[0]);
    }
}