<nav id="main-navigator" class="navigator">
    <div class="sidebar-collapse">
        <a href="#" id="logo"><img src="<?php echo assets_admin_images_url('logo.png'); ?>" alt=""></a>
        <ul class="side-menu lv-1-ul">
            <?php
            $active_class = $menu == MENU_NONE ? '' : $submenu == MENU_NONE ? 'active' : '';
            $collapse_class = $menu == MENU_ADMIN_HOME ? 'collapse in' : 'collapse';
            ?>
            <li class="lv-1-li <?php echo $active_class; ?>">
                <a title="เมนูหลัก">
                    <span class="menu-item-icon">
                        <i class="fa fa-list"></i>
                    </span>
                    <span class="menu-item-title">เมนูหลัก</span>
                </a>

                <div class="sub-menu second <?php echo $collapse_class; ?> hide">
                    <ul class="lv-2-ul">
                        <li class="lv-2-li <?php echo $submenu == MENU_ADMIN_HOME ? 'active' : ''; ?>">
                            <a href="#" title="เมนูย่อย 1">
                                <span class="label-item">เมนูย่อย 1</span>
                            </a>
                        </li>
                        <li class="lv-2-li <?php echo $submenu == MENU_ADMIN_HOME ? 'active' : ''; ?>">
                            <a href="#" title="เมนูย่อย 2">
                                <span class="label-item">เมนูย่อย 2</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php
            $active_class = $menu == MENU_ADMIN_SETTINGS ? 'active' : '';
            $collapse_class = $menu == MENU_ADMIN_SETTINGS ? 'collapse in' : 'collapse';
            ?>
            <li class="lv-1-li <?php echo $active_class; ?>">
                <a href="#" title="ตั้งค่าระบบ">
                    <span class="menu-item-icon">
                        <i class="fa fa-cog"></i>
                    </span>
                    <span class="menu-item-title">ตั้งค่าระบบ</span>
                </a>
            </li>
        </ul>
    </div>
</nav>