<header id="header-panel">
	<a id="menu-btn"><i class="fa fa-bars"></i></a>

	<div id="account-dropdown" class="dropdown navbar-right">
		<a id="account-button" class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			<!-- <?php echo $user_info['account']['username']; ?> -->
			Admin
			<span class="caret"></span>
		</a>
		<ul class="dropdown-menu" aria-labelledby="account-dropdown">
			<li><a href="<?php echo site_url('home/logout'); ?>">ออกจากระบบ</a></li>
		</ul>
	</div>
</header>