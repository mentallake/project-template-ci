<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

	public function index(){
		$menu = MENU_ADMIN_HOME;

		$data = array(
			"menu" => $menu,
		);

		$this->add_data($data);
		$this->load->library('admin_template');
		$this->admin_template->load($this->template_name, 'home_view', $this->data);
	}
}
