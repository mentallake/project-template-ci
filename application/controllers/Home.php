<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends App_Controller {
	public function __construct(){
        // Call the CI_Model constructor
        parent::__construct();
    }

	public function index(){
		$data = array();
		$this->add_data($data);
		$this->load->library('template');
		$this->template->load($this->template_name, 'welcome_message', $this->data);
	}
}
