<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_Model extends CI_Model{
	/**
	 * Constructor
	 */
    public function __construct(){
        parent::__construct();

        date_default_timezone_set('Asia/Bangkok');
    }
}

/* End of file app_model.php */
/* Location: ./application/core/app_model.php */